require('dotenv').config();
const merge = require('lodash.merge');
const { getTermRestrictions, getTermModelRestrictions } = require('@hestia-earth/glossary');

const apiUrl = process.env.API_URL;
if (!apiUrl) {
  throw new Error('Missing required env var "API_URL"');
}

const { join } = require('path');
const pluralize = require('pluralize');
const capitalize = require('capitalize');
const axios = require('axios');
const { isTypeValid } = require('@hestia-earth/schema');

const {
  SRC_DIR, modelsDir, ingoreCache, writeToFile, parseJSONData,
  findTerms, listModels, isUtils, readModelContent, isModel, isInit, parseFileModel, findModel, extraModels
} = require('./utils');

const baseUrl = 'https://hestia.earth';
const schemaUrl = `${baseUrl}/schema`;
const glossaryUrl = `${baseUrl}/glossary`;
const termUrl = `${baseUrl}/term`;
const documentationTag = '"""';

const ignoreFile = file => !isUtils(file) && !file.includes('all.py');

const loadTerm = async id => (await axios.get(`${apiUrl}/terms/${id}`)).data;

const parseDocumentation = (content) => {
  const firstTagIndex = content.indexOf(documentationTag);
  const secondTagIndex = content.indexOf(documentationTag, firstTagIndex + documentationTag.length);
  return content.substring(firstTagIndex, secondTagIndex).replace(documentationTag, '').trim();
};

const joinIndent = (nbIndent = 1) => '\n' + '  '.repeat(nbIndent);

const increaseIndent = (values, nbIndent) => values.split('\n').join(joinIndent(nbIndent));

const isCleanValue = v => [
  !v.includes('>')
].every(Boolean);

const getType = (value) => {
  const type = typeof value === 'object' && '@type' in value ? value['@type'] : null;
  if (!!type && !isTypeValid({ type })) {
    throw new Error(`Invalid @type: "${type}"`);
  }
  return type;
};

const filterKey = key => !['@type', '@doc'].includes(key);

const isStringArray = value => Array.isArray(value) && value.every(v => typeof v === 'string');

const joinStringValues = value => (Array.isArray(value) ? value : [value]).map(v => `\`${v}\``).join(' **or** ');

const lineStart = value => value.length ? `- ${value}` : value;

const schemaLink = {
  '@doc': () => '',
  '@type': () => '',
  '@id': (_k, value, type) => `[@id](https://hestia.earth/schema/${type}#id) must be set (is linked to an existing ${type})`,
  'term.@id': (_k, value, type) => `[term](${schemaUrl}/${type}#term) with ${
    (Array.isArray(value) ? value : [value]).map(term => `[${term}](${termUrl}/${term})`).join(' **or** ')
  }`,
  'term.termType': (_k, value, type) => `[term](${schemaUrl}/${type}#term) of [termType](${schemaUrl}/Term#termType) = ${
    (Array.isArray(value) ? value : [value]).map(v => `[${v}](${glossaryUrl}?termType=${v})`).join(' **or** ')
  }`,
  'term.units': (_k, value, type) => `[term](${schemaUrl}/${type}#term) of [units](${schemaUrl}/Term#units) = ${joinStringValues(value)}`,
  'methodModel.@id': (_k, value, type) => `[methodModel](${schemaUrl}/${type}#methodModel) with ${
    (Array.isArray(value) ? value : [value]).map(term => `[${term}](${termUrl}/${term})`).join(' **or** ')
  }`,
  'key.@id': (_k, value, type) => `[key](${schemaUrl}/${type}#key) with ${
    (Array.isArray(value) ? value : [value]).map(term => `[${term}](${termUrl}/${term})`).join(' **or** ')
  }`,
  'termType': (_k, value, type) => `[termType](${schemaUrl}/Term#termType) = ${
    (Array.isArray(value) ? value : [value]).map(v => `[${v}](${glossaryUrl}?termType=${v})`).join(' **or** ')
  }`,
  'methodModel': (_k, value, type) => `[methodModel](${schemaUrl}/${type}#methodModel) with [${value}](${termUrl}/${value})`,
  'completeness': (key, value, _t, keyLink) => `Data completeness assessment for ${keyLink}: [${key}.${keyLink}](${schemaUrl}/Completeness#${keyLink})${value ? ` must be \`${value}\`` : ''}`,
  'primary': (key, _v, type) => `[primary](${schemaUrl}/${type}#${key}) = \`True\``,
  'default': (key, value, type) =>
    key.includes('.') ?
      schemaLink.completeness(key.split('.')[0], value, type, key.split('.')[1]) :
      `[${key}](${schemaUrl}/${type}#${key}) ${value ? `${isCleanValue(value) ? 'with ' : ''}${joinStringValues(value)}` : ''
      }`
};

/** REQUIREMENTS = */

const requirementsRestrictionByType = {
  siteTypesAllowed: {
    Cycle: siteType => ({ site: { siteType } }),
    Site: siteType => ({ siteType }),
    ImpactAssessment: siteType => ({ site: { siteType } })
  },
  productTermIdsAllowed: {
    Cycle: (ids, productsCount) => ({
      products: [
        ...(new Array(productsCount)),
        { '@type': 'Product', 'primary': 'True', 'term.@id': ids }
      ]
    }),
    Site: ids => ({}),
    ImpactAssessment: ids => ({})
  },
  productTermTypesAllowed: {
    Cycle: (termTypes, productsCount) => ({
      products: [
        ...(new Array(productsCount)),
        { '@type': 'Product', 'primary': 'True', 'term.termType': termTypes }
      ]
    }),
    Site: termTypes => ({}),
    ImpactAssessment: termTypes => ({})
  }
};

const requirementsFromRestrictions = (restrictions, requirements) => {
  const nodeType = Object.keys(requirements)[0];
  const productsCount = nodeType === 'Cycle'
    ? (requirements.Cycle.products || []).find(p => p.primary)
      ? 0
      : (requirements.Cycle.products || []).length
    : 0;
  const data = Object.keys(restrictions).length ?
    {
      [nodeType]: Object.entries(restrictions).reduce((prev, [key, value]) => {
        const req = (!value?.length || value.includes('all'))
          ? {}
          : requirementsRestrictionByType[key]?.[nodeType]?.(value, productsCount);
        return { ...prev, ...req };
      }, {})
    }
    : {};
  return merge(data, requirements);
};

const requirementGroupDocs = (key, value) =>
  Array.isArray(value)
    ? value.map(v => `
    ${typeof v === 'object' && v['@doc'] ? v['@doc'] : 'the following fields'}:
    - ${increaseIndent(requirementGroupDocs(key, v).join(`${joinIndent(1)}- `), 1)}
    `.trim())
    : Object
      .entries(value)
      .filter(([key1]) => filterKey(key1))
      .map(([key1, value1]) => key1 === 'or'
        ? `
  either:
    - ${increaseIndent(requirementGroupDocs(key, value1).join(`${joinIndent(1)}- `), 1)}
      `.trim()
        : key1 === 'optional'
        ? `
  optional:
    - ${increaseIndent(requirementGroupDocs(key, value1).join(`${joinIndent(1)}- `), 1)}
      `.trim()
        : key1 === 'none'
        ? `
  none of:
    - ${increaseIndent(requirementGroupDocs(key, value1).join(`${joinIndent(1)}- `), 1)}
      `.trim()
        : typeof value1 === 'object'
        ? isStringArray(value1)
        ? `a [${key1}](${schemaUrl}/${key}#${key1}) = ${joinStringValues(value1)}`
        : `
    a ${Array.isArray(value1) ? 'list of ' : ''}[${pluralize(key1, Array.isArray(value1) ? 2 : 1)}](${schemaUrl}/${key}#${key1})${
      Object.keys(Array.isArray(value1) ? value1.filter(Boolean)[0] : value1).filter(filterKey).length > 0 ? ' with:' : ''
    }
    ${lineStart(requirementLineDocs(getType(value1) || key, value1))}
        `.trim()
        : schemaLink[key1 in schemaLink ? key1 : 'default'](key1, value1, key).trim()
      );

const requirementLineDocs = (key, value) => {
  return Array.isArray(value)
    ? value.filter(Boolean).map(v =>
      typeof v === 'string'
        ? `- \`${v}\``
        : requirementLineDocs(key, v)
    ).join(`${joinIndent(2)}- `)
    : Object
      .entries(value || {})
      .filter(([k]) => filterKey(k))
      .map(([key1, value1]) =>
        typeof value1 === 'string' || isStringArray(value1)
          ? schemaLink[key1 in schemaLink ? key1 : 'default'](key1, value1, getType(value)).trim()
          : increaseIndent(requirementGroupDocs(getType(value), { [key1]: value1 }).join(joinIndent(1)), 1)
      ).join(' and ')
};

const requirementsDocs = requirements =>
  Object
    .entries(requirements)
    .map(([key, value]) => `
* A [${key}](${schemaUrl}/${key}) with:
  ${lineStart(requirementGroupDocs(key, value).join(`${joinIndent(1)}- `))}
    `.trim())
    .join(`${joinIndent(0)}`);

/** RETURNS = */

const singleEntryAsNode = (value) => {
  const entries = Object.entries(value);
  const [firstEntry] = entries;
  return entries.length === 1
    ? Array.isArray(firstEntry[1]) && firstEntry[1].length === 1 && ('@type' in firstEntry[1][0])
    ? { isArray: true, key: firstEntry[0], value: firstEntry[1], type: firstEntry[1][0]['@type'] }
    : typeof firstEntry[1] === 'object' && '@type' in firstEntry[1]
    ? { isArray: false, key: firstEntry[0], value: firstEntry[1], type: firstEntry[1]['@type'] }
    : null
    : null;
};

const returnTypeDocs = (model, terms, key, value) => {
  const firstEntry = singleEntryAsNode(Array.isArray(value) ? value[0] : value);
  return firstEntry
    ? `
  a ${firstEntry.isArray ? 'list of ' : ''}[${pluralize(firstEntry.key, firstEntry.isArray ? 2 : 1)}](${schemaUrl}/${key}#${firstEntry.key})${
    Object.keys(firstEntry.isArray ? firstEntry.value[0] : firstEntry.value).filter(filterKey).length > 0 ? ' with:' : ''
  }
    ${lineStart(returnTypeDocs(model, terms, firstEntry.type, firstEntry.value).split('\n').join(joinIndent(1)))}
    `.trim()
    : [
      (terms.length ? ['term.@id', terms] : []),
      (key !== 'Term' && isModel(model) ? ['methodModel', model] : []),
      ...(Object.entries(Array.isArray(value) ? value[0] : value))
    ]
      .filter(([k]) => !!k && filterKey(k))
      .map(([key1, value1]) => schemaLink[key1 in schemaLink ? key1 : 'default'](key1, value1, key).trim())
      .join(`${joinIndent(1)}- `);
};

const isSingleReturn = data => Object.keys(data).length === 1 && typeof Object.values(data)[0] === 'string';

const returnsDocs = (returns, model, terms) =>
  Object.keys(returns).length === 0
    ? 'Not implemented'
    : isSingleReturn(returns)
    ? `- ${Object.keys(returns)[0]}`
    : Object
      .entries(returns)
      .map(([key, value]) => `
* A ${Array.isArray(value) ? 'list of ' : ''}[${pluralize(key, Array.isArray(value) ? 2 : 1)}](${schemaUrl}/${key})${
  Object.keys(Array.isArray(value) ? value[0] : value).filter(filterKey).length > 0 ? ' with:' : ''
}
  ${lineStart(returnTypeDocs(model, terms, key, value))}
      `.trim()).join(`${joinIndent(0)}`);

/** LOOKUPS = */

const lookupsFromTermRestrictions = (term, restrictions) => {
  const columns = Object.keys(restrictions).filter(key => restrictions[key].length && !restrictions[key].includes('all'));
  return columns.length ? { [term.termType]: columns } : {};
};

const lookupsFromTermModelRestrictions = (term, restrictions) => {
  const columns = Object.keys(restrictions).filter(key => restrictions[key].length && !restrictions[key].includes('all'));
  return columns.length ? columns.reduce((prev, column) => ({
    ...prev,
    [`${term.termType}-model-${column}`]: ''
  }), {}) : {};
};

const lookupValueEscape = v => ![
  v.includes(' '),
  v.includes('`')
].some(Boolean);

const lookupsDocs = lookups =>
  [
    lookups['@doc'] ? `${lookups['@doc']}:\n` : '',
    Object
      .entries(lookups)
      .filter(([k]) => filterKey(k))
      .map(([key, value]) => `
      - [${key}.csv](${glossaryUrl}/lookups/${key}.csv)${value ? ` -> ${
        (Array.isArray(value) ? value : [value]).map(v => lookupValueEscape(v) ? `\`${v}\`` : v).join('; ')
      }` : ''}
      `.trim())
      .join(joinIndent(0))
  ].filter(Boolean).join(joinIndent(0));

const termDocumentationHeader = (term, hasCustomDocs = false) => {
  const desc = term.description || term.definition;
  return [
    term.name,
    desc || (term.name && !hasCustomDocs ? `This model calculates the ${term.name.toLowerCase()}.` : '')
  ].filter(Boolean).join('\n\n');
};

const processModel = async (filepath) => {
  const { model, runName } = parseFileModel(filepath);
  const content = readModelContent(filepath);
  const terms = findTerms(content);
  const term = terms.length ? await loadTerm(terms[0]) : null;
  const docs = parseDocumentation(content);
  const termDocs = terms.length === 1 || (terms.length > 1 && !docs)
    ? termDocumentationHeader(term, !!docs)
    : '';
  const headerDocs = [termDocs, docs].filter(Boolean).join('\n\n');
  if (!headerDocs.length) {
    throw new Error(`missing documentation for ${filepath}`);
  }
  const returns = parseJSONData(content, 'RETURNS');
  const requirements = parseJSONData(content, 'REQUIREMENTS');
  const restrictions = (term ? getTermRestrictions(term['@id']) : {}) || {};
  const modelRestrictions = (model && term ? getTermModelRestrictions(term['@id'], model) : {}) || {};
  const lookups = {
    ...parseJSONData(content, 'LOOKUPS'),
    ...lookupsFromTermRestrictions(term, restrictions),
    ...lookupsFromTermModelRestrictions(term, modelRestrictions)
  };
  const mdLevel = `#${isInit(filepath) ? '' : '#'}`;
  const documentation = `${mdLevel} ${headerDocs}

${mdLevel}# Returns

${returnsDocs(returns, findModel(content) || model, terms)}

${mdLevel}# Requirements

${requirementsDocs(requirementsFromRestrictions({ ...restrictions, ...modelRestrictions }, requirements))}${
  restrictions.typesAllowed?.length > 1 ? `

This model works on the following Node type with identical requirements:

${restrictions.typesAllowed.map(v => `* [${v}](${schemaUrl}/${v})`).join(`${joinIndent(0)}`)}
`.trimEnd() : ''
}
${Object.keys(lookups).length ? `
${mdLevel}# Lookup used

${lookupsDocs(lookups)}
` : ''}
${mdLevel}# Usage

1. Install the library: \`pip install hestia_earth.models\`
2. Import the library and run the model:

\`\`\`python
from hestia_earth.models.${model} import run

print(run(${runName ? `'${runName}', ` : ''}${Object.keys(requirements).join(', ')}))
\`\`\`
`;
  const outputFile = isInit(filepath) ? filepath.replace('__init__.py', '_model.md') : filepath.replace('.py', '.md');
  writeToFile(join(SRC_DIR, outputFile), documentation);
};

const processFolder = async folder => {
  if (!isModel(folder)) {
    const name = capitalize.words(folder.replace('_', ' '));
    const documentation = `# ${name}

These models are specific to [${name}](https://hestia.earth/schema/${name.replace(' ', '')}).
`;
    writeToFile(join(SRC_DIR, `${folder}/_model.md`), documentation);
    return;
  }
  const data = await loadTerm(folder);
  const documentation = `# ${data.name.replace('& ', '')}

${data.description}
`;
  writeToFile(join(SRC_DIR, `${folder}/_model.md`), documentation);
};

const processValues = async (files, processFunc) => {
  const results = await Promise.all(files.map(async file => {
    try {
      await processFunc(file);
      return { file };
    }
    catch (error) {
      return { file, error };
    }
  }));
  const success = results.filter(({ error }) => !error);
  const errors = results.filter(({ error }) => !!error);
  if (errors.length) {
    errors.map(({ file, error }) => console.error(`File not processed: ${file}`, error.stack));
    console.error('x', 'processed', errors.length, 'files with errors (see above)');
  }
  if (success.length) {
    console.log('✓', 'processed', success.length, 'files successfully');
  }
};

const run = async () => {
  console.log('Processing models...');
  const folders = modelsDir.filter(f => ignoreFile(f) && ingoreCache(f) && !['utils', 'mocking'].includes(f));
  await processValues(folders, processFolder);

  console.log('Processing model keys...');
  const models = listModels(true).filter(ignoreFile);
  await processValues(models, processModel);

  // special case for folders which doesn't contain any sub-models
  console.log('Processing extra models...');
  const extraFiles = extraModels.map(m => [m, '__init__.py'].join('/'));
  await processValues(extraFiles, processModel);
};

run().then(() => process.exit(0)).catch(() => process.exit(1));

require('dotenv').config();
const axios = require('axios');

const { links } = require('../model-links.json');
const API_URL = process.env.API_URL;

const run = async () => {
  const terms = links.map(({ term }) => term).filter(Boolean);
  const query = {
    bool: {
      must: [{
        match: { '@type': 'Term' }
      }],
      should: terms.map(term => ({ match: { '@id.keyword': term } })),
      minimum_should_match: 1
    }
  };
  const { data: { results } } = await axios.post(`${API_URL}/search`, {
    limit: 1000,
    fields: ['@id'],
    query
  });
  const missing = terms.map(term => {
    const exists = results.find(res => res['@id'] === term);
    return exists ? true : term;
  }).filter(val => val !== true);
  if (missing.length) {
    throw new Error(`Terms not found: ${missing.join(', ')}`);
  }
};

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});

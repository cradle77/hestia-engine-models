require('dotenv').config();
const fs = require('fs');
const { join, resolve } = require('path');
const axios = require('axios');
const { writeFile } = fs.promises;

const [type, id] = process.argv.slice(2);
const API_URL = process.env.API_URL;
const ROOT = resolve(join(__dirname, '../'));

const downloadNode = async (nodeType, nodeId) => {
  try {
    const { data } = await axios.get(`${API_URL}/${nodeType.toLowerCase()}s/${nodeId}`, {
      dataState: 'recalculated'
    });
    return data;
  }
  catch (err) {
    console.log(`${API_URL}/${nodeType.toLowerCase()}s/${nodeId}`);
    const { data } = await axios.get(`${API_URL}/${nodeType.toLowerCase()}s/${nodeId}`);
    return data;
  }
};

const run = async () => {
  const node = await downloadNode(type, id);
  if ('cycle' in node) {
    node.cycle = await downloadNode(node.cycle['@type'], node.cycle['@id']);
  }
  if ('site' in node) {
    node.site = await downloadNode(node.site['@type'], node.site['@id']);
  }
  await writeFile(join(ROOT, 'samples', `${id}.jsonld`), JSON.stringify(node, null, 2), 'utf-8');
};

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});

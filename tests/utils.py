import os
import copy
import json
from hestia_earth.schema import SchemaType
from hestia_earth.utils.tools import non_empty_list

fixtures_path = os.path.abspath('tests/fixtures')
TERM = {
    "@type": "Term",
    "@id": "test"
}
CYCLE = {
    "@type": "Cycle",
    "@id": "cycle"
}
SITE = {
    "@type": "Site",
    "@id": "site"
}
SOURCE = {
    "@type": "Source",
    "@id": "source"
}
MEASUREMENT = {
    "@type": "Measurement",
    "term": {
        "@type": "Term",
        "@id": "test"
    }
}
INPUT = {
    "@type": "Input",
    "term": {
        "@type": "Term",
        "@id": "test"
    }
}
PRODUCT = {
    "@type": "Product",
    "term": {
        "@type": "Term",
        "@id": "test"
    }
}
PROPERTY = {
    "@type": "Property",
    "term": {
        "@type": "Term",
        "@id": "test"
    }
}
PRACTICE = {
    "@type": "Practice",
    "term": {
        "@type": "Term",
        "@id": "test"
    }
}
EMISSION = {
    "@type": "Emission",
    "term": {
        "@type": "Term",
        "@id": "test"
    }
}
INDICATOR = {
    "@type": "Indicator",
    "term": {
        "@type": "Term",
        "@id": "test"
    }
}
FLOODED_RICE_TERMS = ['ricePlantFlooded', 'riceGrainInHuskFlooded']


def order_list(values: list): return sorted(values, key=lambda x: x.get('term', {}).get('@id'))


def order_lists(obj: dict, props: list):
    for prop in props:
        if prop in obj:
            obj[prop] = order_list(obj[prop])


def fake_download(node_id, node_type=SchemaType.TERM):
    return {"@type": node_type.value, "@id": node_id}


def fake_download_term(node_id, node_type=SchemaType.TERM):
    return {"@type": node_type.value, "@id": node_id, "name": node_id, "termType": "other"}


def fake_load_impacts(inputs):
    def _load_impact(input: dict):
        impact = input.get('impactAssessment')
        if impact:
            with open(f"{fixtures_path}/impact_assessment/{impact.get('@id')}.jsonld", encoding='utf-8') as f:
                return {**input, 'impactAssessment': json.load(f)}
    return non_empty_list(map(_load_impact, inputs))


def _set_term(node, term):
    if isinstance(term, str):
        node['term']['@id'] = term
    else:
        node['term'] = term


def _set_method(node, model):
    if model is not None:
        node['method'] = {'@type': 'Term', '@id': model}
    return node


def _set_methodModel(node, model):
    if model is not None:
        node['methodModel'] = {'@type': 'Term', '@id': model}
    return node


def _set_model(node, model):
    if model is not None:
        node['model'] = {'@type': 'Term', '@id': model}
    return node


def fake_new_measurement(term, model=None, *args):
    node = copy.deepcopy(MEASUREMENT)
    _set_term(node, term)
    _set_method(node, model)
    return node


def fake_new_input(term, model=None):
    node = copy.deepcopy(INPUT)
    _set_term(node, term)
    _set_model(node, model)
    return node


def fake_new_product(term, value, model=None):
    node = copy.deepcopy(PRODUCT)
    _set_term(node, term)
    _set_model(node, model)
    node['value'] = [value]
    if value == 0:
        node['economicValueShare'] = 0
        node['revenue'] = 0
        node['currency'] = 'USD'
    return node


def fake_new_property(term, model=None):
    node = copy.deepcopy(PROPERTY)
    _set_term(node, term)
    _set_methodModel(node, model)
    return node


def fake_new_practice(term, model=None):
    node = copy.deepcopy(PRACTICE)
    _set_term(node, term)
    _set_model(node, model)
    return node


def fake_new_emission(term, model=None):
    node = copy.deepcopy(EMISSION)
    _set_term(node, term)
    _set_methodModel(node, model)
    return node


def fake_new_indicator(term, model=None):
    node = copy.deepcopy(INDICATOR)
    _set_term(node, term)
    _set_methodModel(node, model)
    return node

from unittest.mock import patch
import json

from tests.utils import fixtures_path, TERM
from hestia_earth.models.utils.product import (
    _new_product, abg_residue_on_field_nitrogen_content, blg_residue_nitrogen
)

class_path = 'hestia_earth.models.utils.product'
fixtures_folder = f"{fixtures_path}/utils/product"


@patch(f"{class_path}._include_model", side_effect=lambda n, x: n)
@patch(f"{class_path}.download_hestia", return_value=TERM)
def test_new_product(*args):
    # with a Term as string
    product = _new_product('term', 10)
    assert product == {
        '@type': 'Product',
        'term': TERM,
        'value': [10]
    }

    # with a Term as dict
    product = _new_product(TERM, 10)
    assert product == {
        '@type': 'Product',
        'term': TERM,
        'value': [10]
    }

    # no value
    product = _new_product(TERM)
    assert product == {
        '@type': 'Product',
        'term': TERM,
        'value': [0],
        'economicValueShare': 0,
        'revenue': 0,
        'currency': 'USD'
    }


def test_abg_residue_on_field_nitrogen_content_no_products():
    assert abg_residue_on_field_nitrogen_content([]) == 0


def test_abg_residue_on_field_nitrogen_content():
    with open(f"{fixtures_folder}/products-cropResidue.jsonld", encoding='utf-8') as f:
        products = json.load(f)

    assert abg_residue_on_field_nitrogen_content(products) == 0.8445757894736851


def test_blg_residue_nitrogen_no_products():
    assert blg_residue_nitrogen([]) == 0


def test_blg_residue_nitrogen():
    with open(f"{fixtures_folder}/products-cropResidue.jsonld", encoding='utf-8') as f:
        products = json.load(f)

    assert blg_residue_nitrogen(products) == 13.606542431999996

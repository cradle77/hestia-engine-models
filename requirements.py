from dotenv import load_dotenv
load_dotenv()


import argparse
import json

from hestia_earth.models.requirements import get_all, get_single, list_models, get_term_ids, get_single_returns

parser = argparse.ArgumentParser()
parser.add_argument('--output-file', type=str, default='console',
                    help='Filepath to save the result to as JSON. If not set, will print to console.')

parser.add_argument('--debug', action='store_true',
                    help='Show the list of models found.')
parser.add_argument('--list-models-only', action='store_true',
                    help='Show only the list of models found (does not return the requirements).')
parser.add_argument('--return-ids-only', action='store_true',
                    help='Returns the list of Term `@id` the models would gap-fill. Can be used with --term-type.')
parser.add_argument('--model-returns-only', action='store_true',
                    help='Returns the data returned by a model. Only works with with --model and --model-key.')
parser.add_argument('--model', type=str,
                    help='Filter by model @id. Must be used with --model-key.')
parser.add_argument('--model-key', type=str,
                    help='Filter by model key. Must be used with --model.')
parser.add_argument('--term-type', type=str,
                    help='Select models by termType (e.g. Blank Node). Does not work if used with --model.')
parser.add_argument('--tier', type=str,
                    help='Select models by tier (Emissions only). Does not work if used with --model.')
parser.add_argument('--site-type', type=str,
                    help='Remove models not running for this "site.siteType". Does not work if used with --model.')
parser.add_argument('--product-term-type', type=str,
                    help='Remove models not running for this "product.term.termType". Does not work if used with --model.')
parser.add_argument('--product-term-id', type=str,
                    help='Remove models not running for this "product.term.@id". Does not work if used with --model.')
args = parser.parse_args()


def main():
    if args.return_ids_only:
        term_ids = get_term_ids(
            termType=args.term_type,
            tier=args.tier,
            productTermId=args.product_term_id,
            productTermType=args.product_term_type,
            siteType=args.site_type
        )
        print(f"{len(term_ids)} @id found")
        print('\n'.join(term_ids))
        return
    if args.list_models_only:
        models = list_models(
            termType=args.term_type,
            tier=args.tier,
            productTermId=args.product_term_id,
            productTermType=args.product_term_type,
            siteType=args.site_type
        )
        print(f"{len(models)} models found")
        for model in models:
            print('/'.join([model.get('model'), model.get('key')]))
        return
    if args.model_returns_only and args.model and args.model_key:
        returns = get_single_returns(args.model, args.model_key)
        print(returns)
        return

    requirements = get_single(args.model, args.model_key) if args.model and args.model_key else get_all(
        debug=args.debug,
        tier=args.tier,
        productTermId=args.product_term_id,
        productTermType=args.product_term_type,
        siteType=args.site_type
    )

    if args.output_file and args.output_file.endswith('.json'):
        with open(args.output_file, 'w') as f:
            json.dump(requirements, f, indent=2)
    else:
        print(json.dumps(requirements, indent=2))


if __name__ == "__main__":
    main()

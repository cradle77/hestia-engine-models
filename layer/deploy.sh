#!/bin/bash

# exit when any command fails
set -e

STAGE=${1:-"dev"}
PKG_PATH=./python/lib/python3.9/site-packages

cd ./layer/

# make sure we use the latest version of the files
cp -R ../hestia_earth $PKG_PATH/.
# remove tests
rm -rf $PKG_PATH/tests
# remove numpy as already included in another layer
rm -rf $PKG_PATH/numpy*

rm -rf layer.zip
zip -r layer.zip python

aws lambda publish-layer-version \
    --region us-east-1 \
    --layer-name "hestia-$STAGE-python39-engine-models" \
    --description "Engine Models running on python 3.9" \
    --zip-file "fileb://layer.zip" \
    --compatible-runtimes python3.9

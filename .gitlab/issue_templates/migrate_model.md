# Migrate an existing Model

This task is to migrate a model from this source file:
<!--
Add the link to the source of the model here.
Example:
- https://gitlab.com/hestia-earth/hestia-calculation-engine/-/blob/develop/hestia_earth/calculation/emissions/ch4ToAirCropResidueBurningAkagiEtAl2011AndIpcc2006.py
- https://gitlab.com/hestia-earth/hestia-gap-filling-engine/-/blob/develop/hestia_earth/gap_filling/cycle_models/ag_crop_residue.py
-->

## Tasks

- [ ] Create a branch
- [ ] Migrate the code
- [ ] Migrate the documentation
- [ ] Migrate the unit tests
- [ ] Create a Merge Request

/label ~feature
/label ~refactoring

# IPCC (2021)

These models, described in the [IPCC (2021)](https://www.ipcc.ch/assessment-report/ar6/) guidelines, including the [supplementary material](https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_Chapter_07_Supplementary_Material.pdf), characterise different greenhouse gases into a global warming or global temperature potential.

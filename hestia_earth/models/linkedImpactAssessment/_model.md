# Linked Impact Assessment

A model which takes recalculated data from an Impact Assessment linked to an Input in a Cycle.

## Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [linkedImpactAssessment](https://hestia.earth/term/linkedImpactAssessment)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `background`
  - [statsDefinition](https://hestia.earth/schema/Emission#statsDefinition) with `modelled`
  - [inputs](https://hestia.earth/schema/Emission#inputs)
  - [operation](https://hestia.earth/schema/Emission#operation)

## Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [value](https://hestia.earth/schema/Input#value) `> 0` and [impactAssessment](https://hestia.earth/schema/Input#impactAssessment)

## Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.linkedImpactAssessment import run

print(run('all', Cycle))
```

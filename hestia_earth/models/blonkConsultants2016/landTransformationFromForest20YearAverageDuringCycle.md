## Land transformation, from forest, 20 year average, during Cycle

The amount of land used by this Cycle, that changed use from forest to the current use in the last 20 years, divided by 20.

### Returns

* A list of [Indicators](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [landTransformationFromForest20YearAverageDuringCycle](https://hestia.earth/term/landTransformationFromForest20YearAverageDuringCycle)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [blonkConsultants2016](https://hestia.earth/term/blonkConsultants2016)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType)
  - a [product](https://hestia.earth/schema/ImpactAssessment#product)
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - a list of [products](https://hestia.earth/schema/Cycle#products) with:
      - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) `> 0` and [economicValueShare](https://hestia.earth/schema/Product#economicValueShare) `> 0` and either:
      - if the [cycle.functionalUnit](https://hestia.earth/schema/Cycle#functionalUnit) = 1 ha, additional properties are required:
        - [cycleDuration](https://hestia.earth/schema/Cycle#cycleDuration)
        - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [longFallowRatio](https://hestia.earth/term/longFallowRatio)
      - for orchard crops, additional properties are required:
        - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
          - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) with [saplings](https://hestia.earth/term/saplings)
        - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [nurseryDuration](https://hestia.earth/term/nurseryDuration)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [orchardBearingDuration](https://hestia.earth/term/orchardBearingDuration)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [orchardDensity](https://hestia.earth/term/orchardDensity)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [orchardDuration](https://hestia.earth/term/orchardDuration)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [rotationDuration](https://hestia.earth/term/rotationDuration)

### Lookup used

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `cropGroupingFaostatArea`
- [region-crop-cropGroupingFaostatArea-landTransformation20YearsAverage.csv](https://hestia.earth/glossary/lookups/region-crop-cropGroupingFaostatArea-landTransformation20YearsAverage.csv) -> use crop grouping above or default to site.siteType
- [resourceUse.csv](https://hestia.earth/glossary/lookups/resourceUse.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.blonkConsultants2016 import run

print(run('landTransformationFromForest20YearAverageDuringCycle', ImpactAssessment))
```

## N2O, to air, crop residue decomposition, indirect

Nitrous oxide emissions to air, indirectly created from NOx, NH3, and NO3 emissions, from crop residue decomposition.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [n2OToAirCropResidueDecompositionIndirect](https://hestia.earth/term/n2OToAirCropResidueDecompositionIndirect)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [ipcc2006](https://hestia.earth/term/ipcc2006)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`
  - [statsDefinition](https://hestia.earth/schema/Emission#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/null#siteType) with `cropland` **or** `glass or high accessible cover`
  - Data completeness assessment for cropResidue: [completeness.cropResidue](https://hestia.earth/schema/Completeness#cropResidue) must be `True`
  - a list of [emissions](https://hestia.earth/schema/Cycle#emissions) with:
    - [value](https://hestia.earth/schema/Emission#value) and [term](https://hestia.earth/schema/Emission#term) with [no3ToGroundwaterCropResidueDecomposition](https://hestia.earth/term/no3ToGroundwaterCropResidueDecomposition)
    - [value](https://hestia.earth/schema/Emission#value) and [term](https://hestia.earth/schema/Emission#term) with [nh3ToAirCropResidueDecomposition](https://hestia.earth/term/nh3ToAirCropResidueDecomposition)
    - [value](https://hestia.earth/schema/Emission#value) and [term](https://hestia.earth/schema/Emission#term) with [noxToAirCropResidueDecomposition](https://hestia.earth/term/noxToAirCropResidueDecomposition)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2006 import run

print(run('n2OToAirCropResidueDecompositionIndirect', Cycle))
```

## N2O, to air, organic soil cultivation, direct

Direct nitrous oxide emissions to air, from organic soil (histosol) cultivation.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [n2OToAirOrganicSoilCultivationDirect](https://hestia.earth/term/n2OToAirOrganicSoilCultivationDirect)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [ipcc2006](https://hestia.earth/term/ipcc2006)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`
  - [statsDefinition](https://hestia.earth/schema/Emission#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `forest` **or** `other natural vegetation` **or** `cropland` **or** `glass or high accessible cover` **or** `permanent pasture` and a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [histosol](https://hestia.earth/term/histosol)
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [ecoClimateZone](https://hestia.earth/term/ecoClimateZone)
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) `> 0` and [economicValueShare](https://hestia.earth/schema/Product#economicValueShare) `> 0`
  - optional:
    - [cycleDuration](https://hestia.earth/schema/Cycle#cycleDuration)

### Lookup used

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `isOrchard`
- [ecoClimateZone.csv](https://hestia.earth/glossary/lookups/ecoClimateZone.csv) -> `IPCC_2006_ORGANIC_SOILS_KG_N2O-N_HECTARE`
- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2006 import run

print(run('n2OToAirOrganicSoilCultivationDirect', Cycle))
```

# IPCC (2006)

These models, described in the [IPCC (2006)](https://www.ipcc.ch/report/2006-ipcc-guidelines-for-national-greenhouse-gas-inventories/) guidelines, calculate direct and indirect greenhouse gas emissions and provide data for lookup tables.

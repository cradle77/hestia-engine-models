## NH3, to air, excreta

Ammonia emissions to air, from animal excreta.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [nh3ToAirExcreta](https://hestia.earth/term/nh3ToAirExcreta)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [emeaEea2019](https://hestia.earth/term/emeaEea2019)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 2`
  - [statsDefinition](https://hestia.earth/schema/Emission#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/null#siteType) with `cropland` **or** `permanent pasture` **or** `animal housing` **or** `pond` **or** `river or stream` **or** `lake` **or** `sea or ocean`
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [excreta](https://hestia.earth/glossary?termType=excreta) and [term](https://hestia.earth/schema/Input#term) of [units](https://hestia.earth/schema/Term#units) = `kg N` and a list of [properties](https://hestia.earth/schema/Input#properties) with:
      - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [totalAmmoniacalNitrogenContentAsN](https://hestia.earth/term/totalAmmoniacalNitrogenContentAsN)
  - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
    - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) of [termType](https://hestia.earth/schema/Term#termType) = [excretaManagement](https://hestia.earth/glossary?termType=excretaManagement)

This model works on the following Node type with identical requirements:

* [Cycle](https://hestia.earth/schema/Cycle)
* [Transformation](https://hestia.earth/schema/Transformation)

### Lookup used

- [excretaManagement-excreta-NH3_EF_2019.csv](https://hestia.earth/glossary/lookups/excretaManagement-excreta-NH3_EF_2019.csv) -> product @id
- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.emeaEea2019 import run

print(run('nh3ToAirExcreta', Cycle))
```

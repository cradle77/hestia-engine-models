## N2O, to air, fuel combustion, direct

Nitrous oxide emissions to air, from the combustion of fuel.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [n2OToAirFuelCombustionDirect](https://hestia.earth/term/n2OToAirFuelCombustionDirect)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [emeaEea2019](https://hestia.earth/term/emeaEea2019)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`
  - [statsDefinition](https://hestia.earth/schema/Emission#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - either:
    - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
      - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [fuel](https://hestia.earth/glossary?termType=fuel)
    - Data completeness assessment for electricityFuel: [completeness.electricityFuel](https://hestia.earth/schema/Completeness#electricityFuel) must be `True`

This model works on the following Node type with identical requirements:

* [Cycle](https://hestia.earth/schema/Cycle)
* [Transformation](https://hestia.earth/schema/Transformation)
* [Transport](https://hestia.earth/schema/Transport)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.emeaEea2019 import run

print(run('n2OToAirFuelCombustionDirect', Cycle))
```

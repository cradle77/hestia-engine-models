# EMEA-EEA (2019)

These models implements the methods in the EMEP-EEA Handbook (2019), including fuel combustion emissions (Part B, Chapter 1.A.4, page 22).

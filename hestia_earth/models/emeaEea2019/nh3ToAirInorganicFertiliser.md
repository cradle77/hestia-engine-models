## NH3, to air, inorganic fertiliser

Ammonia emissions to air, from inorganic fertiliser volatilization.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [nh3ToAirInorganicFertiliser](https://hestia.earth/term/nh3ToAirInorganicFertiliser)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [emeaEea2019](https://hestia.earth/term/emeaEea2019)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`
  - [statsDefinition](https://hestia.earth/schema/Emission#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `glass or high accessible cover` **or** `permanent pasture` and a [country](https://hestia.earth/schema/Site#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region) and a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [soilPh](https://hestia.earth/term/soilPh)
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [temperatureAnnual](https://hestia.earth/term/temperatureAnnual)
  - either:
    - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
      - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [inorganicFertiliser](https://hestia.earth/glossary?termType=inorganicFertiliser) **or** [inorganicNitrogenFertiliserUnspecifiedKgN](https://hestia.earth/glossary?termType=inorganicNitrogenFertiliserUnspecifiedKgN)
    - Data completeness assessment for fertiliser: [completeness.fertiliser](https://hestia.earth/schema/Completeness#fertiliser) must be `True`

### Lookup used

- [inorganicFertiliser.csv](https://hestia.earth/glossary/lookups/inorganicFertiliser.csv) -> `NH3_emissions_factor_acidic`; `NH3_emissions_factor_basic`
- [region-inorganicFertiliser-fertGroupingNitrogen-breakdown.csv](https://hestia.earth/glossary/lookups/region-inorganicFertiliser-fertGroupingNitrogen-breakdown.csv)
- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.emeaEea2019 import run

print(run('nh3ToAirInorganicFertiliser', Cycle))
```

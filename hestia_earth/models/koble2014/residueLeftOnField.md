## Residue left on field

The share of above ground crop residue left on the field surface.

### Returns

* A list of [Practices](https://hestia.earth/schema/Practice) with:
  - [term](https://hestia.earth/schema/Practice#term) with [residueLeftOnField](https://hestia.earth/term/residueLeftOnField)
  - [methodModel](https://hestia.earth/schema/Practice#methodModel) with [koble2014](https://hestia.earth/term/koble2014)
  - [value](https://hestia.earth/schema/Practice#value)
  - [statsDefinition](https://hestia.earth/schema/Practice#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/null#siteType) with `cropland` **or** `glass or high accessible cover` **or** `permanent pasture`
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [primary](https://hestia.earth/schema/Product#primary) = `True` and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) **or** [forage](https://hestia.earth/glossary?termType=forage)
  - Data completeness assessment for cropResidue: [completeness.cropResidue](https://hestia.earth/schema/Completeness#cropResidue) must be `False`

### Lookup used

- [cropResidueManagement.csv](https://hestia.earth/glossary/lookups/cropResidueManagement.csv) -> `siteTypesAllowed`; `productTermTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.koble2014 import run

print(run('residueLeftOnField', Cycle))
```

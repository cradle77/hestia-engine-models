## Freshwater eutrophication potential

The potential of nutrient emissions to cause excessive growth of aquatic plants and algae in freshwater ecosystems (e.g. lakes, rivers, streams). Freshwater eutrophication is primarily linked to phosphorus as this tends to be the limiting nutrient in these ecosystems.

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [freshwaterEutrophicationPotential](https://hestia.earth/term/freshwaterEutrophicationPotential)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [recipe2016Individualist](https://hestia.earth/term/recipe2016Individualist)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) of [termType](https://hestia.earth/schema/Term#termType) = [emission](https://hestia.earth/glossary?termType=emission)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `pEqIndividualistFreshwaterEutrophicationReCiPe2016`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.recipe2016Individualist import run

print(run('freshwaterEutrophicationPotential', ImpactAssessment))
```

# Akagi et al (2011) and IPCC (2006)

This model calculates the emissions from crop residue burning, using the methodology detailed in the IPCC (2006, Volume 4, Chapter 2, Section 2.4) guidelines and the emissions factors detailed in Akagi et al (2011, Atmos. Chem. Phys., 11, 4039–4072).

## Land transformation, from permanent pasture, 20 year average, during Cycle

The amount of land used by this Cycle, that changed use from permanent pasture to the current use in the last 20 years, divided by 20.

### Returns

* A list of [Indicators](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [landTransformationFromPermanentPasture20YearAverageDuringCycle](https://hestia.earth/term/landTransformationFromPermanentPasture20YearAverageDuringCycle)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [hyde32](https://hestia.earth/term/hyde32)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
    - [siteType](https://hestia.earth/schema/null#siteType) with `cropland` **or** `glass or high accessible cover` **or** `animal housing` **or** `pond` **or** `agri-food processor` **or** `food retailer`
  - either:
    - a [country](https://hestia.earth/schema/ImpactAssessment#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
    - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
      - a [region](https://hestia.earth/schema/Site#region) with:
        - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
  - [endDate](https://hestia.earth/schema/ImpactAssessment#endDate)
  - a [product](https://hestia.earth/schema/ImpactAssessment#product)
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - either:
      - if the [cycle.functionalUnit](https://hestia.earth/schema/Cycle#functionalUnit) = 1 ha, additional properties are required:
        - [cycleDuration](https://hestia.earth/schema/Cycle#cycleDuration)
        - a list of [products](https://hestia.earth/schema/Cycle#products) with:
          - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) `> 0` and [economicValueShare](https://hestia.earth/schema/Product#economicValueShare) `> 0`
        - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [longFallowRatio](https://hestia.earth/term/longFallowRatio)
      - for orchard crops, additional properties are required:
        - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
          - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) with [saplings](https://hestia.earth/term/saplings)
        - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [nurseryDuration](https://hestia.earth/term/nurseryDuration)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [orchardBearingDuration](https://hestia.earth/term/orchardBearingDuration)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [orchardDensity](https://hestia.earth/term/orchardDensity)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [orchardDuration](https://hestia.earth/term/orchardDuration)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [rotationDuration](https://hestia.earth/term/rotationDuration)

### Lookup used

One of (depending on `site.siteType`):

- [region-cropland-landTransformation20years.csv](https://hestia.earth/glossary/lookups/region-cropland-landTransformation20years.csv) -> `permanent pasture`
- [region-forest-landTransformation20years.csv](https://hestia.earth/glossary/lookups/region-forest-landTransformation20years.csv) -> `permanent pasture`
- [region-other_natural_vegetation-landTransformation20years.csv](https://hestia.earth/glossary/lookups/region-other_natural_vegetation-landTransformation20years.csv) -> `permanent pasture`
- [resourceUse.csv](https://hestia.earth/glossary/lookups/resourceUse.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.hyde32 import run

print(run('landTransformationFromPermanentPasture20YearAverageDuringCycle', ImpactAssessment))
```

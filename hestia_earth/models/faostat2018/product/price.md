## Product Price

Calculates the price of `crop` and `liveAnimal` using FAOSTAT data.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - [methodModel](https://hestia.earth/schema/Product#methodModel) with [faostat2018](https://hestia.earth/term/faostat2018)
  - [price](https://hestia.earth/schema/Product#price)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) **or** [animalProduct](https://hestia.earth/glossary?termType=animalProduct) **or** [liveAnimal](https://hestia.earth/glossary?termType=liveAnimal)
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - a [country](https://hestia.earth/schema/Site#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)

### Lookup used

Depending on the primary product [termType](https://hestia.earth/schema/Product#term):

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `cropGroupingFaostatProduction`
- [region-crop-cropGroupingFaostatProduction-price.csv](https://hestia.earth/glossary/lookups/region-crop-cropGroupingFaostatProduction-price.csv) -> use value from above
- [liveAnimal.csv](https://hestia.earth/glossary/lookups/liveAnimal.csv) -> `primaryMeatProductFAO`
- [animalProduct.csv](https://hestia.earth/glossary/lookups/animalProduct.csv) -> `animalProductGroupingFAOEquivalent`; `animalProductGroupingFAO`; `liveAnimal`
- [region-animalProduct-animalProductGroupingFAO-price.csv](https://hestia.earth/glossary/lookups/region-animalProduct-animalProductGroupingFAO-price.csv) -> use value from above
- [region-animalProduct-animalProductGroupingFAO-averageColdCarcassWeight.csv](https://hestia.earth/glossary/lookups/region-animalProduct-animalProductGroupingFAO-averageColdCarcassWeight.csv) -> use value from above

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.faostat2018 import run

print(run('product.price', Cycle))
```

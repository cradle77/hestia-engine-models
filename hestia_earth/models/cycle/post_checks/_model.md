## Cycle Post Checks

List of models to run **after** any other model on an `Cycle`.

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import primary
from hestia_earth.models.cycle import post_checks

product = primary.run(cycle)
cycle = post_checks.run(cycle)
print(cycle)
```

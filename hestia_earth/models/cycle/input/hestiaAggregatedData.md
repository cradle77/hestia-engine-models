## Input Hestia Aggregated Data

This model adds `impactAssessment` to `Input` based on data which has been aggregated into country level averages.
Note: to get more accurate impacts, we recommend setting the
[input.impactAssessment](https://hestia.earth/schema/Input#impactAssessment)
instead of the region-level averages using this model.

### Returns

* A list of [Inputs](https://hestia.earth/schema/Input) with:
  - [impactAssessment](https://hestia.earth/schema/Input#impactAssessment) with `added from Hestia`
  - [impactAssessmentIsProxy](https://hestia.earth/schema/Input#impactAssessmentIsProxy) with `True`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [value](https://hestia.earth/schema/Input#value) and none of:
      - [impactAssessment](https://hestia.earth/schema/Input#impactAssessment) and optional:
      - a [country](https://hestia.earth/schema/Input#country) with:
        - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
      - a [region](https://hestia.earth/schema/Input#region) with:
        - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
  - optional:
    - a [site](https://hestia.earth/schema/Cycle#site) with:
      - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `pasture` and a [country](https://hestia.earth/schema/Site#country) with:
        - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
    - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
      - [term](https://hestia.earth/schema/Input#term) with [seed](https://hestia.earth/term/seed) and [value](https://hestia.earth/schema/Input#value) and none of:
        - [impactAssessment](https://hestia.earth/schema/Input#impactAssessment)
    - a list of [products](https://hestia.earth/schema/Cycle#products) with:
      - [value](https://hestia.earth/schema/Product#value) and [primary](https://hestia.earth/schema/Product#primary) = `True`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('input.hestiaAggregatedData', Cycle))
```

## Cycle Pre Checks Start Date

This model calculates the
[startDate](https://hestia.earth/schema/Cycle#startDate) from the
[endDate](https://hestia.earth/schema/Cycle#endDate) and
[cycleDuration](https://hestia.earth/schema/Cycle#cycleDuration).

### Returns

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - [startDate](https://hestia.earth/schema/Cycle#startDate)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - [endDate](https://hestia.earth/schema/Cycle#endDate)
  - [cycleDuration](https://hestia.earth/schema/Cycle#cycleDuration)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run(Cycle))
```

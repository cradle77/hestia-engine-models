## Completeness Crop Residue

This model checks if we have the requirements below and updates the
[Data Completeness](https://hestia.earth/schema/Completeness#cropResidue) value.

### Returns

* A [Completeness](https://hestia.earth/schema/Completeness) with:
  - [cropResidue](https://hestia.earth/schema/Completeness#cropResidue)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - Data completeness assessment for cropResidue: [completeness.cropResidue](https://hestia.earth/schema/Completeness#cropResidue) must be `False`
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) with [belowGroundCropResidue](https://hestia.earth/term/belowGroundCropResidue)
    - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueTotal](https://hestia.earth/term/aboveGroundCropResidueTotal)
  - optional:
    - a list of [products](https://hestia.earth/schema/Cycle#products) with:
      - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved)
      - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated)
      - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt)
      - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueLeftOnField](https://hestia.earth/term/aboveGroundCropResidueLeftOnField)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('completeness.cropResidue', Cycle))
```

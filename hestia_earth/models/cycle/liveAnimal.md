## Live Animal

This model calculates the amount of live animal produced during a Cycle, based on the amount of animal product.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [liveAnimal](https://hestia.earth/glossary?termType=liveAnimal)
  - [value](https://hestia.earth/schema/Product#value)
  - [statsDefinition](https://hestia.earth/schema/Product#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [animalProduct](https://hestia.earth/glossary?termType=animalProduct) and a list of [properties](https://hestia.earth/schema/Product#properties) with:
      - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [coldCarcassWeightPerHead](https://hestia.earth/term/coldCarcassWeightPerHead) **or** [coldDressedCarcassWeightPerHead](https://hestia.earth/term/coldDressedCarcassWeightPerHead) **or** [liveweightPerHead](https://hestia.earth/term/liveweightPerHead) **or** [readyToCookWeightPerHead](https://hestia.earth/term/readyToCookWeightPerHead)
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `animal housing` **or** `permanent pasture`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('liveAnimal', Cycle))
```

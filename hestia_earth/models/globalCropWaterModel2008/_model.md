# Global Crop Water Model (2008)

This model adds rooting depths based on the Global Crop Water Model [Siebert and Doll (2008)](https://www.uni-frankfurt.de/45217788/FHP_07_Siebert_and_Doell_2008.pdf).

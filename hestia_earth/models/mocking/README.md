# Hestia Engine Models Mocking

When deploying the calculations models on your own server, you might want to bypass any calls to the Hestia API.
You can use this mocking functionality in that purpose.

```python
from hestia_earth.models.pooreNemecek2018.aboveGroundCropResidueTotal import run
from hestia_earth.models.mocking import enable_mock

enable_mock()
run(cycle)
```

## Potential evapotranspiration (long-term annual mean)

The long-term average annual potential evapotranspiration, averaged over all years in the recent climate record.

### Returns

* A list of [Measurements](https://hestia.earth/schema/Measurement) with:
  - [term](https://hestia.earth/schema/Measurement#term) with [potentialEvapotranspirationLongTermAnnualMean](https://hestia.earth/term/potentialEvapotranspirationLongTermAnnualMean)
  - [value](https://hestia.earth/schema/Measurement#value)
  - [startDate](https://hestia.earth/schema/Measurement#startDate)
  - [endDate](https://hestia.earth/schema/Measurement#endDate)
  - [statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`
  - [methodClassification](https://hestia.earth/schema/Measurement#methodClassification) with `geospatial dataset`

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:
  - a [siteType](https://hestia.earth/schema/Site#siteType) = `forest` **or** `other natural vegetation` **or** `cropland` **or** `glass or high accessible cover` **or** `permanent pasture` **or** `animal housing` **or** `pond` **or** `river or stream` **or** `lake` **or** `sea or ocean` **or** `agri-food processor`
  - either:
    - the following fields:
      - [latitude](https://hestia.earth/schema/Site#latitude)
      - [longitude](https://hestia.earth/schema/Site#longitude)
    - the following fields:
      - a [boundary](https://hestia.earth/schema/Site#boundary)
    - the following fields:
      - a [region](https://hestia.earth/schema/Site#region) with:
        - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)

### Lookup used

- [measurement.csv](https://hestia.earth/glossary/lookups/measurement.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.geospatialDatabase import run

print(run('potentialEvapotranspirationLongTermAnnualMean', Site))
```

## Cropping intensity

A metric of multi-cropping. If calculated from area, it is the maximum monthly growing area divided by the total cropland area.

### Returns

* A list of [Practices](https://hestia.earth/schema/Practice) with:
  - [term](https://hestia.earth/schema/Practice#term) with [croppingIntensity](https://hestia.earth/term/croppingIntensity)
  - [value](https://hestia.earth/schema/Practice#value)
  - [statsDefinition](https://hestia.earth/schema/Practice#statsDefinition) with `spatial`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `glass or high accessible cover` and either:
      - the following fields:
        - [latitude](https://hestia.earth/schema/Site#latitude)
        - [longitude](https://hestia.earth/schema/Site#longitude)
      - the following fields:
        - a [boundary](https://hestia.earth/schema/Site#boundary)
      - the following fields:
        - a [region](https://hestia.earth/schema/Site#region) with:
          - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [primary](https://hestia.earth/schema/Product#primary) = `True` and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop)

### Lookup used

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `isOrchard`
- [region-landUseManagement.csv](https://hestia.earth/glossary/lookups/region-landUseManagement.csv) -> `croppingIntensity`
- [landUseManagement.csv](https://hestia.earth/glossary/lookups/landUseManagement.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.geospatialDatabase import run

print(run('croppingIntensity', Cycle))
```

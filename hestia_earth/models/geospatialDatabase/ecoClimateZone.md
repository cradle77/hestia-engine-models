## Eco-Climate Zone

A grouping areas based on their ecology and climate, defined by the JRC.

They approximataely map to the [IPCC (2019) Climate Zones](https://www.ipcc-nggip.iges.or.jp/public/2019rf/pdf/4_Volume4/19R_V4_Ch03_Land%20Representation.pdf).
Data are derived from [Hiederer et al. (2010) Biofuels: A new methodology to estimate GHG emissions from global land use change, European Commission Joint Research Centre](https://ec.europa.eu/jrc/en/publication/eur-scientific-and-technical-research-reports/biofuels-new-methodology-estimate-ghg-emissions-due-global-land-use-change-methodology).

| Value | Climate Zone         |
|-------|----------------------|
| 1     | Warm Temperate Moist |
| 2     | Warm Temperate Dry   |
| 3     | Cool Temperate Moist |
| 4     | Cool Temperate Dry   |
| 5     | Polar Moist          |
| 6     | Polar Dry            |
| 7     | Boreal Moist         |
| 8     | Boreal Dry           |
| 9     | Tropical Montane     |
| 10    | Tropical Wet         |
| 11    | Tropical Moist       |
| 12    | Tropical Dry         |

### Returns

* A list of [Measurements](https://hestia.earth/schema/Measurement) with:
  - [term](https://hestia.earth/schema/Measurement#term) with [ecoClimateZone](https://hestia.earth/term/ecoClimateZone)
  - [value](https://hestia.earth/schema/Measurement#value)
  - [description](https://hestia.earth/schema/Measurement#description)
  - [statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`
  - [methodClassification](https://hestia.earth/schema/Measurement#methodClassification) with `geospatial dataset`

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:
  - a [siteType](https://hestia.earth/schema/Site#siteType) = `forest` **or** `other natural vegetation` **or** `cropland` **or** `glass or high accessible cover` **or** `permanent pasture` **or** `animal housing` **or** `pond` **or** `river or stream` **or** `lake` **or** `sea or ocean` **or** `agri-food processor`
  - either:
    - the following fields:
      - [latitude](https://hestia.earth/schema/Site#latitude)
      - [longitude](https://hestia.earth/schema/Site#longitude)
    - the following fields:
      - a [boundary](https://hestia.earth/schema/Site#boundary)
    - the following fields:
      - a [region](https://hestia.earth/schema/Site#region) with:
        - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)

### Lookup used

- [region-measurment.csv](https://hestia.earth/glossary/lookups/region-measurment.csv) -> `ecoClimateZone`
- [measurement.csv](https://hestia.earth/glossary/lookups/measurement.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.geospatialDatabase import run

print(run('ecoClimateZone', Site))
```

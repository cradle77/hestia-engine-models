## Potential evapotranspiration (annual)

The total annual potential evapotranspiration, expressed in mm / year.

Must be associated with at least 1 `Cycle` that has an
[endDate](https://hestia.earth/schema/Cycle#endDate) after `1958` and before `2021`.

### Returns

* A list of [Measurements](https://hestia.earth/schema/Measurement) with:
  - [term](https://hestia.earth/schema/Measurement#term) with [potentialEvapotranspirationAnnual](https://hestia.earth/term/potentialEvapotranspirationAnnual)
  - [value](https://hestia.earth/schema/Measurement#value)
  - [startDate](https://hestia.earth/schema/Measurement#startDate)
  - [endDate](https://hestia.earth/schema/Measurement#endDate)
  - [statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`
  - [methodClassification](https://hestia.earth/schema/Measurement#methodClassification) with `geospatial dataset`

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:
  - a [siteType](https://hestia.earth/schema/Site#siteType) = `forest` **or** `other natural vegetation` **or** `cropland` **or** `glass or high accessible cover` **or** `permanent pasture` **or** `animal housing` **or** `pond` **or** `river or stream` **or** `lake` **or** `sea or ocean` **or** `agri-food processor`
  - either:
    - the following fields:
      - [latitude](https://hestia.earth/schema/Site#latitude)
      - [longitude](https://hestia.earth/schema/Site#longitude)
    - the following fields:
      - a [boundary](https://hestia.earth/schema/Site#boundary)
    - the following fields:
      - a [region](https://hestia.earth/schema/Site#region) with:
        - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)

### Lookup used

- [measurement.csv](https://hestia.earth/glossary/lookups/measurement.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.geospatialDatabase import run

print(run('potentialEvapotranspirationAnnual', Site))
```

## Input Value

This model calculates the `value` of the [Input](https://hestia.earth/schema/Input)
by taking the value of the same [Product](https://hestia.earth/schema/Product) of the Cycle and applying the
[share](https://hestia.earth/schema/Transformation/transformedShare).

### Returns

* A list of [Transformations](https://hestia.earth/schema/Transformation) with:
  - a list of [inputs](https://hestia.earth/schema/Transformation#inputs) with:
    - [value](https://hestia.earth/schema/Input#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [value](https://hestia.earth/schema/Product#value)
  - a list of [transformations](https://hestia.earth/schema/Cycle#transformations) with:
    - [transformedShare](https://hestia.earth/schema/Transformation#transformedShare) and a list of [inputs](https://hestia.earth/schema/Transformation#inputs)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.transformation import run

print(run('input.value', Cycle))
```

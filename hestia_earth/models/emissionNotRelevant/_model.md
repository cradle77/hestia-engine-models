# Emission not relevant

The emission is not relevant for this Site, Cycle, or Product.

## Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [emissionNotRelevant](https://hestia.earth/term/emissionNotRelevant)
  - [value](https://hestia.earth/schema/Emission#value) with `0`
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `not relevant`

## Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [emissions](https://hestia.earth/schema/Cycle#emissions)

## Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.emissionNotRelevant import run

print(run('all', Cycle))
```

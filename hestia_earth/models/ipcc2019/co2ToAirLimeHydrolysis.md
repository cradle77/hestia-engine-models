## CO2, to air, lime hydrolysis

Carbon dioxide emissions to air, from lime hydrolysis (including dolomitic lime).

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [co2ToAirLimeHydrolysis](https://hestia.earth/term/co2ToAirLimeHydrolysis)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [ipcc2019](https://hestia.earth/term/ipcc2019)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`
  - [statsDefinition](https://hestia.earth/schema/Emission#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/null#siteType) with `cropland` **or** `glass or high accessible cover` **or** `permanent pasture`
  - Data completeness assessment for soilAmendments: [completeness.soilAmendments](https://hestia.earth/schema/Completeness#soilAmendments)
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [soilAmendment](https://hestia.earth/glossary?termType=soilAmendment) and [term](https://hestia.earth/schema/Input#term) of [units](https://hestia.earth/schema/Term#units) = `kg CaCO3` **or** `kg MgCO3`

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2019 import run

print(run('co2ToAirLimeHydrolysis', Cycle))
```

## Below ground crop residue

The total amount of below ground crop residue as dry matter. Properties can be added, such as the nitrogen composition.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - [term](https://hestia.earth/schema/Product#term) with [belowGroundCropResidue](https://hestia.earth/term/belowGroundCropResidue)
  - [methodModel](https://hestia.earth/schema/Product#methodModel) with [ipcc2019](https://hestia.earth/term/ipcc2019)
  - [value](https://hestia.earth/schema/Product#value)
  - [statsDefinition](https://hestia.earth/schema/Product#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/null#siteType) with `cropland` **or** `glass or high accessible cover` **or** `permanent pasture`
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [primary](https://hestia.earth/schema/Product#primary) = `True` and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) **or** [forage](https://hestia.earth/glossary?termType=forage) and optional:
      - a list of [properties](https://hestia.earth/schema/Product#properties) with:
        - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [dryMatter](https://hestia.earth/term/dryMatter)
  - Data completeness assessment for cropResidue: [completeness.cropResidue](https://hestia.earth/schema/Completeness#cropResidue) must be `False`

### Lookup used

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `IPCC_2019_Ratio_AGRes_YieldDM`; `IPCC_2019_Ratio_BGRes_AGRes`
- [cropResidue.csv](https://hestia.earth/glossary/lookups/cropResidue.csv) -> `siteTypesAllowed`; `productTermTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2019 import run

print(run('belowGroundCropResidue', Cycle))
```

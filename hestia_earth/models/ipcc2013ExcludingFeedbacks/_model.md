# IPCC (2013) excluding feedbacks

These models, described in the [IPCC (2013)](https://www.ipcc.ch/assessment-report/ar5/) guidelines, characterise different greenhouse gases into a global warming or global temperature potential. Conversions from each gas to CO2 equivalents exclude climate carbon feedbacks. Climate carbon feedbacks were not included in the IPCC (2007) or earlier guidelines, and were first provided in the [IPCC (2013)](https://www.ipcc.ch/site/assets/uploads/2018/02/WG1AR5_Chapter08_FINAL.pdf) guidelines in Table 8.7.

## Product Value

Returns the [value](https://hestia.earth/schema/Product#value) of the Product linked to the Cycle.

### Returns

- The product value as a number

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - a list of [products](https://hestia.earth/schema/Cycle#products) with:
      - [value](https://hestia.earth/schema/Product#value)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.impact_assessment import run

print(run('productValue', ImpactAssessment))
```

## Primary Product

The `product` of an [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment)
is the [primary](https://hestia.earth/schema/Product#primary) product of the `Cycle`.

### Returns

* A [Term](https://hestia.earth/schema/Term)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.impact_assessment import run

print(run('product', ImpactAssessment))
```

## Freshwater withdrawals, during Cycle

Withdrawals of water from freshwater lakes, rivers, and aquifers that occur during the Cycle.

### Returns

* A list of [Indicators](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [freshwaterWithdrawalsDuringCycle](https://hestia.earth/term/freshwaterWithdrawalsDuringCycle)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [product](https://hestia.earth/schema/ImpactAssessment#product)
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - a list of [products](https://hestia.earth/schema/Cycle#products) with:
      - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) `> 0` and [economicValueShare](https://hestia.earth/schema/Product#economicValueShare) `> 0` and either:
      - Data completeness assessment for water: [completeness.water](https://hestia.earth/schema/Completeness#water) must be `True`
      - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
        - [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [water](https://hestia.earth/glossary?termType=water) and [value](https://hestia.earth/schema/Input#value) `> 0`

### Lookup used

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `cropGroupingFAO`
- [region.csv](https://hestia.earth/glossary/lookups/region.csv) -> `Conveyancing_Efficiency_Annual_crops`; `Conveyancing_Efficiency_Permanent_crops`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.impact_assessment import run

print(run('freshwaterWithdrawalsDuringCycle', ImpactAssessment))
```

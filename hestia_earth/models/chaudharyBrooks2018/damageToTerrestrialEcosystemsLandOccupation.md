## Damage to terrestrial ecosystems, land occupation

The fraction of species richness that may be potentially lost in terrestrial ecosystems due to land occupation. See [lc-impact.eu](https://lc-impact.eu/EQland_stress.html).

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [damageToTerrestrialEcosystemsLandOccupation](https://hestia.earth/term/damageToTerrestrialEcosystemsLandOccupation)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [chaudharyBrooks2018](https://hestia.earth/term/chaudharyBrooks2018)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) and either:
      - [ecoregion](https://hestia.earth/schema/Site#ecoregion)
      - a [country](https://hestia.earth/schema/Site#country) with:
        - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - a list of [products](https://hestia.earth/schema/Cycle#products) with:
      - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) `> 0` and [economicValueShare](https://hestia.earth/schema/Product#economicValueShare) `> 0` and either:
      - if the [cycle.functionalUnit](https://hestia.earth/schema/Cycle#functionalUnit) = 1 ha, additional properties are required:
        - [cycleDuration](https://hestia.earth/schema/Cycle#cycleDuration)
        - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [longFallowRatio](https://hestia.earth/term/longFallowRatio)
      - for orchard crops, additional properties are required:
        - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
          - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) with [saplings](https://hestia.earth/term/saplings)
        - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [nurseryDuration](https://hestia.earth/term/nurseryDuration)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [orchardBearingDuration](https://hestia.earth/term/orchardBearingDuration)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [orchardDensity](https://hestia.earth/term/orchardDensity)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [orchardDuration](https://hestia.earth/term/orchardDuration)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [rotationDuration](https://hestia.earth/term/rotationDuration)

### Lookup used

Different lookup files are used depending on the situation:

- [ecoregion-siteType-LandOccupationChaudaryBrooks2018CF.csv](https://hestia.earth/glossary/lookups/ecoregion-siteType-LandOccupationChaudaryBrooks2018CF.csv) -> using `ecoregion`
- [region-siteType-LandOccupationChaudaryBrooks2018CF.csv](https://hestia.earth/glossary/lookups/region-siteType-LandOccupationChaudaryBrooks2018CF.csv) -> using `country`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.chaudharyBrooks2018 import run

print(run('damageToTerrestrialEcosystemsLandOccupation', ImpactAssessment))
```

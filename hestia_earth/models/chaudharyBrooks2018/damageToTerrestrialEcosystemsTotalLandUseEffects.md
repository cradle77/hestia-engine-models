## Damage to terrestrial ecosystems, total land use effects

The fraction of species richness that may be potentially lost in terrestrial ecosystems due to land occupation and transformation. See [lc-impact.eu](https://lc-impact.eu/EQland_stress.html).

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [damageToTerrestrialEcosystemsTotalLandUseEffects](https://hestia.earth/term/damageToTerrestrialEcosystemsTotalLandUseEffects)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [chaudharyBrooks2018](https://hestia.earth/term/chaudharyBrooks2018)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - a list of [products](https://hestia.earth/schema/Cycle#products) with:
      - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) `> 0` and [economicValueShare](https://hestia.earth/schema/Product#economicValueShare) `> 0`
  - a list of [impacts](https://hestia.earth/schema/ImpactAssessment#impacts) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) with [damageToTerrestrialEcosystemsLandOccupation](https://hestia.earth/term/damageToTerrestrialEcosystemsLandOccupation)
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) with [damageToTerrestrialEcosystemsLandTransformation](https://hestia.earth/term/damageToTerrestrialEcosystemsLandTransformation)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.chaudharyBrooks2018 import run

print(run('damageToTerrestrialEcosystemsTotalLandUseEffects', ImpactAssessment))
```

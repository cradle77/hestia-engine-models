# Chaudhary Brooks (2018)

This model calculates the biodiversity impacts related to habitat loss, accounting for different land use intensities, as defined in Chaudhary & Brooks (2018, Environ. Sci. Technol. 52, 9, 5094–5104).

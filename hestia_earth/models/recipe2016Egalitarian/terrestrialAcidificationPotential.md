## Terrestrial acidification potential

Changes in soil chemical properties following the deposition of nitrogen and sulfur in acidifying forms.

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [terrestrialAcidificationPotential](https://hestia.earth/term/terrestrialAcidificationPotential)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [recipe2016Egalitarian](https://hestia.earth/term/recipe2016Egalitarian)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) of [termType](https://hestia.earth/schema/Term#termType) = [emission](https://hestia.earth/glossary?termType=emission)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `so2EqEgalitarianTerrestrialAcidificationReCiPe2016`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.recipe2016Egalitarian import run

print(run('terrestrialAcidificationPotential', ImpactAssessment))
```

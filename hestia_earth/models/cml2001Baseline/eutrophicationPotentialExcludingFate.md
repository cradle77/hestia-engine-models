## Eutrophication potential, excluding fate

The potential of nutrient emissions to cause excessive growth of aquatic plants and algae in aquatic ecosystems.

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [eutrophicationPotentialExcludingFate](https://hestia.earth/term/eutrophicationPotentialExcludingFate)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [cml2001Baseline](https://hestia.earth/term/cml2001Baseline)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) of [termType](https://hestia.earth/schema/Term#termType) = [emission](https://hestia.earth/glossary?termType=emission)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `po4-EqEutrophicationExcludingFateCml2001Baseline`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cml2001Baseline import run

print(run('eutrophicationPotentialExcludingFate', ImpactAssessment))
```

## Precipitation (annual)

The total annual precipitation (defined as the sum of rainfall, sleet, snow, and hail, but excluding fog, cloud, and dew), expressed in mm / year.

Compute Annual value based on Monthly values.

### Returns

* A list of [Measurements](https://hestia.earth/schema/Measurement) with:
  - [term](https://hestia.earth/schema/Measurement#term) with [precipitationAnnual](https://hestia.earth/term/precipitationAnnual)
  - [value](https://hestia.earth/schema/Measurement#value)
  - [startDate](https://hestia.earth/schema/Measurement#startDate)
  - [endDate](https://hestia.earth/schema/Measurement#endDate)
  - [statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `modelled`
  - [methodClassification](https://hestia.earth/schema/Measurement#methodClassification) with `modelled using other physical measurements`

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:
  - a [siteType](https://hestia.earth/schema/Site#siteType) = `forest` **or** `other natural vegetation` **or** `cropland` **or** `glass or high accessible cover` **or** `permanent pasture` **or** `animal housing` **or** `pond` **or** `river or stream` **or** `lake` **or** `sea or ocean` **or** `agri-food processor`
  - a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
    - Data completeness assessment for id: [term.id](https://hestia.earth/schema/Completeness#id) must be `precipitationMonthly`

### Lookup used

- [measurement.csv](https://hestia.earth/glossary/lookups/measurement.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.site import run

print(run('precipitationAnnual', Site))
```

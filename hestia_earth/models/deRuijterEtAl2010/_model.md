# de Ruijter et al (2010)

This model calculates the NH3 emissions due to crop residue decomposition using the regression model in de Ruijter et al (2010, Atmospheric Environment, 44, 28, 3362-3368).

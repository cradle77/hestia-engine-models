## Damage to human health, water stress

The disability-adjusted life years lost in the human population due to water stress. See [lc-impact.eu](https://lc-impact.eu/HHwater_stress.html).

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [damageToHumanHealthWaterStress](https://hestia.earth/term/damageToHumanHealthWaterStress)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [lcImpactAllEffectsInfinite](https://hestia.earth/term/lcImpactAllEffectsInfinite)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) of [termType](https://hestia.earth/schema/Term#termType) = [resourceUse](https://hestia.earth/glossary?termType=resourceUse)
  - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
    - either:
      - [awareWaterBasinId](https://hestia.earth/schema/Site#awareWaterBasinId)
      - a [country](https://hestia.earth/schema/Site#country) with:
        - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)

### Lookup used

Different lookup files are used depending on the situation:

- [awareWaterBasinId-resourceUse-WaterStressDamageToHumanHealthLCImpactCF.csv](https://hestia.earth/glossary/lookups/awareWaterBasinId-resourceUse-WaterStressDamageToHumanHealthLCImpactCF.csv) -> using `awareWaterBasinId`
- [region-resourceUse-WaterStressDamageToHumanHealthLCImpactCF.csv](https://hestia.earth/glossary/lookups/region-resourceUse-WaterStressDamageToHumanHealthLCImpactCF.csv) -> using `region`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.lcImpactAllEffectsInfinite import run

print(run('damageToHumanHealthWaterStress', ImpactAssessment))
```

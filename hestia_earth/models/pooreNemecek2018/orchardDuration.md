## Orchard duration

The length, in days, from the establishment of an orchard to its removal.

### Returns

* A list of [Practices](https://hestia.earth/schema/Practice) with:
  - [term](https://hestia.earth/schema/Practice#term) with [orchardDuration](https://hestia.earth/term/orchardDuration)
  - [methodModel](https://hestia.earth/schema/Practice#methodModel) with [pooreNemecek2018](https://hestia.earth/term/pooreNemecek2018)
  - [value](https://hestia.earth/schema/Practice#value)
  - [statsDefinition](https://hestia.earth/schema/Practice#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland`
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop)

### Lookup used

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `Orchard_duration`
- [landUseManagement.csv](https://hestia.earth/glossary/lookups/landUseManagement.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.pooreNemecek2018 import run

print(run('orchardDuration', Cycle))
```

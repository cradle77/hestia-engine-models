## CH4, to air, aquaculture systems

Methane emissions to air, from the methanogenesis of organic carbon in excreta, unconsumed feed, fertiliser, and net primary production. Reaches the air through diffusion and ebullition.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [ch4ToAirAquacultureSystems](https://hestia.earth/term/ch4ToAirAquacultureSystems)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [pooreNemecek2018](https://hestia.earth/term/pooreNemecek2018)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`
  - [statsDefinition](https://hestia.earth/schema/Emission#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `pond` **or** `river or stream` **or** `lake` **or** `sea or ocean` and a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [temperatureAnnual](https://hestia.earth/term/temperatureAnnual)
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [waterDepth](https://hestia.earth/term/waterDepth) and either:
      - [siteType](https://hestia.earth/schema/Site#siteType) with `sea or ocean`
      - a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
        - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [slowFlowingWater](https://hestia.earth/term/slowFlowingWater)
        - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [fastFlowingWater](https://hestia.earth/term/fastFlowingWater)
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [excreta](https://hestia.earth/glossary?termType=excreta) and [term](https://hestia.earth/schema/Product#term) of [units](https://hestia.earth/schema/Term#units) = `kg VS`
    - [primary](https://hestia.earth/schema/Product#primary) = `True` and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [liveAquaticSpecies](https://hestia.earth/glossary?termType=liveAquaticSpecies)
  - [endDate](https://hestia.earth/schema/Cycle#endDate)
  - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
    - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [excretionIntoWaterBody](https://hestia.earth/term/excretionIntoWaterBody)
    - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [slaughterAge](https://hestia.earth/term/slaughterAge)
    - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [yieldOfPrimaryAquacultureProductLiveweightPerM2](https://hestia.earth/term/yieldOfPrimaryAquacultureProductLiveweightPerM2)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `productTermTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.pooreNemecek2018 import run

print(run('ch4ToAirAquacultureSystems', Cycle))
```

## Excreta (kg VS)

This model calculates the Excreta (kg VS) from the products as described in
[Poore & Nemecek (2018)](https://science.sciencemag.org/content/360/6392/987).
The model computes it as the balance between the carbon in the inputs plus the carbon produced in the pond
minus the carbon contained in the primary product.
If the mass balance fails
(i.e. [animal feed](https://hestia.earth/schema/Completeness#animalFeed) is not complete, see requirements below),
the fomula is = total excreta as N / [Volatile solids content](https://hestia.earth/term/volatileSolidsContent).

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - [methodModel](https://hestia.earth/schema/Product#methodModel) with [pooreNemecek2018](https://hestia.earth/term/pooreNemecek2018)
  - [value](https://hestia.earth/schema/Product#value)
  - [statsDefinition](https://hestia.earth/schema/Product#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - either:
    - the following fields:
      - Data completeness assessment for animalFeed: [completeness.animalFeed](https://hestia.earth/schema/Completeness#animalFeed)
      - Data completeness assessment for products: [completeness.products](https://hestia.earth/schema/Completeness#products)
      - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
        - [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) **or** [animalProduct](https://hestia.earth/glossary?termType=animalProduct) **or** [feedFoodAdditive](https://hestia.earth/glossary?termType=feedFoodAdditive) and [term](https://hestia.earth/schema/Input#term) of [units](https://hestia.earth/schema/Term#units) = `kg` and [value](https://hestia.earth/schema/Input#value) `> 0` and [isAnimalFeed](https://hestia.earth/schema/Input#isAnimalFeed) with `True` and optional:
          - a list of [properties](https://hestia.earth/schema/Input#properties) with:
            - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [carbonContent](https://hestia.earth/term/carbonContent)
            - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [energyContentHigherHeatingValue](https://hestia.earth/term/energyContentHigherHeatingValue)
      - a list of [products](https://hestia.earth/schema/Cycle#products) with:
        - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) and a list of [properties](https://hestia.earth/schema/Product#properties) with:
          - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [carbonContent](https://hestia.earth/term/carbonContent)
      - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
        - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [slaughterAge](https://hestia.earth/term/slaughterAge)
        - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [yieldOfPrimaryAquacultureProductLiveweightPerM2](https://hestia.earth/term/yieldOfPrimaryAquacultureProductLiveweightPerM2)
      - a [site](https://hestia.earth/schema/Cycle#site) with:
        - a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
          - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [netPrimaryProduction](https://hestia.earth/term/netPrimaryProduction)
    - the following fields:
      - a list of [products](https://hestia.earth/schema/Cycle#products) with:
        - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [animalProduct](https://hestia.earth/glossary?termType=animalProduct) **or** [liveAnimal](https://hestia.earth/glossary?termType=liveAnimal) **or** [liveAquaticSpecies](https://hestia.earth/glossary?termType=liveAquaticSpecies)

### Lookup used

- [crop-property.csv](https://hestia.earth/glossary/lookups/crop-property.csv) -> `carbonContent`; `energyContentHigherHeatingValue`
- [animalProduct.csv](https://hestia.earth/glossary/lookups/animalProduct.csv) -> `excretaKgVsTermId`
- [liveAnimal.csv](https://hestia.earth/glossary/lookups/liveAnimal.csv) -> `excretaKgVsTermId`
- [liveAquaticSpecies.csv](https://hestia.earth/glossary/lookups/liveAquaticSpecies.csv) -> `excretaKgVsTermId`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.pooreNemecek2018 import run

print(run('excretaKgVs', Cycle))
```

## NH3, to air, aquaculture systems

Ammonia emissions to air, created from the breakdown of excreta and unconsumed feed in aquaculture systems.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [nh3ToAirAquacultureSystems](https://hestia.earth/term/nh3ToAirAquacultureSystems)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [pooreNemecek2018](https://hestia.earth/term/pooreNemecek2018)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`
  - [statsDefinition](https://hestia.earth/schema/Emission#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/null#siteType) with `pond` **or** `river or stream` **or** `lake` **or** `sea or ocean`
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [primary](https://hestia.earth/schema/Product#primary) = `True` and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [liveAquaticSpecies](https://hestia.earth/glossary?termType=liveAquaticSpecies)
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [excreta](https://hestia.earth/glossary?termType=excreta) and [term](https://hestia.earth/schema/Input#term) of [units](https://hestia.earth/schema/Term#units) = `kg N` and a list of [properties](https://hestia.earth/schema/Input#properties) with:
      - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [nitrogenContent](https://hestia.earth/term/nitrogenContent)
      - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [totalAmmoniacalNitrogenContentAsN](https://hestia.earth/term/totalAmmoniacalNitrogenContentAsN)
  - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
    - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) of [termType](https://hestia.earth/schema/Term#termType) = [excretaManagement](https://hestia.earth/glossary?termType=excretaManagement)
  - either:
    - a [site](https://hestia.earth/schema/Cycle#site) with:
      - [siteType](https://hestia.earth/schema/Site#siteType) with `pond` **or** `sea or ocean`
    - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
      - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [yieldOfPrimaryAquacultureProductLiveweightPerM2](https://hestia.earth/term/yieldOfPrimaryAquacultureProductLiveweightPerM2)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `productTermTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.pooreNemecek2018 import run

print(run('nh3ToAirAquacultureSystems', Cycle))
```

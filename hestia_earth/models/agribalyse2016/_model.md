# Agribalyse (2016)

These models use data from the [Agribalyse](https://agribalyse.ademe.fr/) dataset to gap fill average values.

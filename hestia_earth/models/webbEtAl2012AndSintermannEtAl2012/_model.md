# Webb et al (2012) and Sintermann et al (2012)

This model calculates the NH3 emissions due to the addition of organic fertiliser based on a compilation of emissions factors from Webb et al (2012, E. Lichtfouse (ed.), Agroecology and Strategies for Climate Change) and Sintermann et al (2012, Biogeosciences, 9, 1611–1632, 2012). The methodology for compiling these emissions is detailed in [Poore & Nemecek (2018)](https://dx.doi.org/10.1126/science.aaq0216).

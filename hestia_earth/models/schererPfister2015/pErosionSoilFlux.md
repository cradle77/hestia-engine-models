## P, erosion, soil flux

Phosphorus losses from soil erosion.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [pErosionSoilFlux](https://hestia.earth/term/pErosionSoilFlux)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [schererPfister2015](https://hestia.earth/term/schererPfister2015)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`
  - [statsDefinition](https://hestia.earth/schema/Emission#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `forest` **or** `other natural vegetation` **or** `cropland` **or** `glass or high accessible cover` **or** `permanent pasture` and a [country](https://hestia.earth/schema/Site#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region) and a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [nutrientLossToAquaticEnvironment](https://hestia.earth/term/nutrientLossToAquaticEnvironment)
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [heavyWinterPrecipitation](https://hestia.earth/term/heavyWinterPrecipitation)
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [totalPhosphorusPerKgSoil](https://hestia.earth/term/totalPhosphorusPerKgSoil)
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [totalNitrogenPerKgSoil](https://hestia.earth/term/totalNitrogenPerKgSoil)
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [precipitationAnnual](https://hestia.earth/term/precipitationAnnual)
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [erodibility](https://hestia.earth/term/erodibility)
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [slopeLength](https://hestia.earth/term/slopeLength)
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [slope](https://hestia.earth/term/slope)
  - [endDate](https://hestia.earth/schema/Cycle#endDate)
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [water](https://hestia.earth/glossary?termType=water)

### Lookup used

- [region.csv](https://hestia.earth/glossary/lookups/region.csv) -> `P_EF_C1`; `EF_P_C2`; `Practice_Factor`
- [tillage.csv](https://hestia.earth/glossary/lookups/tillage.csv) -> `C2_FACTORS`
- [organicFertiliser.csv](https://hestia.earth/glossary/lookups/organicFertiliser.csv) -> `OrganicFertiliserClassification`
- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.schererPfister2015 import run

print(run('pErosionSoilFlux', Cycle))
```

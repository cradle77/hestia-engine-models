## Eutrophication potential, including fate, average Europe

The potential of nutrient emissions to cause excessive growth of aquatic plants and algae in aquatic ecosystems, including an estimated fate of these emissions in Europe.

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [eutrophicationPotentialIncludingFateAverageEurope](https://hestia.earth/term/eutrophicationPotentialIncludingFateAverageEurope)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [cml2001NonBaseline](https://hestia.earth/term/cml2001NonBaseline)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) of [termType](https://hestia.earth/schema/Term#termType) = [emission](https://hestia.earth/glossary?termType=emission)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `noxeqEutrophicationIncludingFateAverageEuropeCml2001Non-Baseline`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cml2001NonBaseline import run

print(run('eutrophicationPotentialIncludingFateAverageEurope', ImpactAssessment))
```

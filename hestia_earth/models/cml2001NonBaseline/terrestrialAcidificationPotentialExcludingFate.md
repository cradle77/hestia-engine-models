## Terrestrial acidification potential, excluding fate

Changes in soil chemical properties following the deposition of nitrogen and sulfur in acidifying forms, excluding average fate of the emissions in Europe.

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [terrestrialAcidificationPotentialExcludingFate](https://hestia.earth/term/terrestrialAcidificationPotentialExcludingFate)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [cml2001NonBaseline](https://hestia.earth/term/cml2001NonBaseline)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) of [termType](https://hestia.earth/schema/Term#termType) = [emission](https://hestia.earth/glossary?termType=emission)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `so2EqTerrestrialAcidificationExcludingFateCml2001Non-Baseline`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cml2001NonBaseline import run

print(run('terrestrialAcidificationPotentialExcludingFate', ImpactAssessment))
```

FROM python:3.9

WORKDIR /app

RUN pip install python-dotenv

COPY requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY requirements-ci.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY . /app

CMD run.py
